# Reachable workspace analysis repo

A simple repo for reachable workspace analysis of different robots using pinocchio library


##  Instalation

Install all dependencies using anaconda

```shell
conda env create -f env.yaml
```

Then just activate the environment

```shell
conda activate reachable_workspace
```

##  Running the example notebook

Run the notebook with jupyter lab
```shell
jupyter lab
```

##  Running the code
Once the example is run the meshcat window will open with Panda robot's workspace visualisation for the horizon time of

```python
horizon = 0.2 # seconds
```
![](imgs/panda.png)


If horizon is changed to 1sec
```python
horizon = 1 # seconds
```
![](imgs/panda_1s.png)


If a different robot is used (`ur5`) for example
```python
robot = load('ur5')
```
![](imgs/ur5.png)


##  The algorithm parameters

The algorithm calculates the reachable set of cartesian positions of the desired frame of the robot given the robots joint position and joint velocity limits.
The output of the algorithm is the reachable space that the robot is able to reach within the horizon time, while guaranteeing that the joint position and velocity limits are not violated.

If you are interested in the complete workspace of the robot, you can set a large time horizon (>1 second)

The parameters of the algorithm are set using the options dictionary. The following options are available:
- frame_name: The name of the frame for which to compute the reachable set. The default value is None, which means that the reachable set is computed for the origin of the robot.
- n_samples: The number of samples to use for the discretization of the joint velocity space. The higher the number of samples, the more accurate the reachable set will be, however the longer the computation time will be
- facet_dim: The dimension of the facet that will be sampled. Between 0 and the number of DOF of the robot.  The higher the number of samples, the more accurate the reachable set will be, however the longer the computation time will be
- convex: Whether to make the reachable set convex or not. If set to True, the reachable set will be convex, if False the reachable set will be non-convex. 
